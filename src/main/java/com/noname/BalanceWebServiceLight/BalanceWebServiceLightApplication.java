package com.noname.BalanceWebServiceLight;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BalanceWebServiceLightApplication {

	public static void main(String[] args) {
		SpringApplication.run(BalanceWebServiceLightApplication.class, args);
	}
}
