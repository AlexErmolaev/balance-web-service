package com.noname.BalanceWebServiceLight.controller;


import com.noname.BalanceWebServiceLight.entities.BalanceData;
import com.noname.BalanceWebServiceLight.service.BalanceDataService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.Arrays;


@RestController
@RequestMapping("/balanceData")
public class BalanceDataController implements BalanceDataService {


    @RequestMapping(
            path = "/solve",
            method = RequestMethod.POST
            )
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<String> solveBalance(@RequestParam("flow") double[] flow, @RequestParam("equalities") double[][] equalities,
                                               @RequestParam("toleranceCrude") double[] toleranceCrude) {
        double[][] equalitiesNatiral = new double[equalities.length/flow.length][flow.length];
        for (int i = 0; i < equalitiesNatiral.length; i++){
            for (int j = 0; j < equalitiesNatiral[0].length; j++){
                equalitiesNatiral[i][j] = equalities[j+equalitiesNatiral[0].length*i][0];
            }
        }                               // ПРИ ЗАПРОСЕ ДВУМЕРНЫЙ МАССИВ EQUALITIES ПРЕОБРАЗУЕТСЯ В МАССИВ, ГДЕ В КАЖДОЙ СТРОКЕ 1 ЗНАЧЕНИЕ(А ДОЛЖЕН БЫТЬ МАССИВ УСЛОВИЙ!!!)
        BalanceData bD = new BalanceData(flow, equalitiesNatiral, toleranceCrude);
        if (bD.flow.length < 1) {
            return ResponseEntity.badRequest()
                    .body("Empty flow");
        }

        if (bD.equalities.length < 1) {
            return ResponseEntity.badRequest()
                    .body("Empty equalities");
        }

        if (bD.toleranceCrude.length < 1) {
            return ResponseEntity.badRequest()
                    .body("Empty tolerance crude");
        }
        String solve = Arrays.toString(BalanceDataService.solveBalance(bD));
        if (solve.length() > 0) {
            return ResponseEntity.ok().body(solve);
        } else return ResponseEntity.badRequest().body("Cannot solving balance");
    }
}
