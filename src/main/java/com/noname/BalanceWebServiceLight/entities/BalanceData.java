package com.noname.BalanceWebServiceLight.entities;

public class BalanceData {
    
    public double[] flow;
    public double[][] equalities;
    public double[] toleranceCrude;

    public BalanceData(double[] flow, double[][] equalities, double[] toleranceCrude) {
        this.flow = flow;
        this.equalities = equalities;
        this.toleranceCrude = toleranceCrude;
    }
}
