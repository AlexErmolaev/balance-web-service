package com.noname.BalanceWebServiceLight.service;

import cern.colt.matrix.DoubleFactory1D;
import cern.colt.matrix.DoubleFactory2D;
import cern.colt.matrix.DoubleMatrix1D;
import cern.colt.matrix.DoubleMatrix2D;
import cern.colt.matrix.linalg.Algebra;
import com.joptimizer.functions.ConvexMultivariateRealFunction;
import com.joptimizer.functions.LinearMultivariateRealFunction;
import com.joptimizer.functions.PSDQuadraticMultivariateRealFunction;
import com.joptimizer.optimizers.JOptimizer;
import com.joptimizer.optimizers.OptimizationRequest;
import com.noname.BalanceWebServiceLight.entities.BalanceData;
import org.apache.log4j.Logger;

public interface BalanceDataService {

    static double[] solveBalance(BalanceData bD){

        if (bD.flow.length > 0 && bD.equalities.length > 0 && bD.toleranceCrude.length > 0) {
            Algebra a = new Algebra();
            double[] W1 = new double[bD.flow.length];
            for (int i = 0; i < W1.length; i++) {
                W1[i] = 1 / Math.pow(bD.toleranceCrude[i], 2);
            }
            DoubleMatrix2D W = DoubleFactory2D.dense.diagonal(DoubleFactory1D.dense.make(W1));

            double[] I0 = new double[bD.flow.length];
            for (int i = 0; i < I0.length; i++) {
                I0[i] = 1;
            }
            DoubleMatrix2D I = DoubleFactory2D.dense.diagonal(DoubleFactory1D.dense.make(I0));

            DoubleMatrix2D H = a.mult(I, W);
            double[] H0 = new double[bD.flow.length];
            for (int i = 0; i < H0.length; i++) {
                H0[i] = 1;
            }
            DoubleMatrix2D H2 = a.mult(H, DoubleFactory2D.dense.diagonal(DoubleFactory1D.dense.make(H0))); // Умножение матрицы H на единичную(можно поменять 1 на 0,1 и ответ будет в 10 раз больше)
            DoubleMatrix1D matrixX0 = DoubleFactory1D.dense.make(bD.flow);
            DoubleMatrix1D d = a.mult(a.mult(H2, I), matrixX0); // d=H*x0, если d=-H*x0, то результирующий поток похож на входящий
            PSDQuadraticMultivariateRealFunction objectiveFunction = new PSDQuadraticMultivariateRealFunction(H2.toArray(), d.toArray(), 0);

            //inequalities (polyhedral feasible set G.X<H )

            ConvexMultivariateRealFunction[] inequalities = new ConvexMultivariateRealFunction[bD.equalities.length];
            for (int i = 0; i < bD.equalities.length; i++) {
                inequalities[i] = new LinearMultivariateRealFunction(bD.equalities[i], 0.0); // Система линейных неравенств где сумма(разность) некоторых Xi <=0
            }

            //optimization problem
            OptimizationRequest or = new OptimizationRequest();
            or.setF0(objectiveFunction);
            or.setFi(inequalities);
            or.setToleranceFeas(1.E-9);
            or.setTolerance(1.E-9);

            //optimization
            Logger logger = Logger.getLogger("");
            JOptimizer opt = new JOptimizer();
            opt.setOptimizationRequest(or);
            try {
                opt.optimize();
            } catch (Exception e) {
                logger.info(e);
            }
            if (opt.getOptimizationResponse() == null) {
                logger.info("Empty optimization response");
                return new double[]{};
            } else {
                double[] sol = opt.getOptimizationResponse().getSolution();
                for (int i = 0; i < sol.length; i++) {
                    sol[i] = sol[i] * (-1);
                }
                return sol;
            }
        } else {
            return new double[]{};
        }
    };
}
