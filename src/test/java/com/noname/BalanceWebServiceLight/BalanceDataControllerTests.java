package com.noname.BalanceWebServiceLight;


import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

class BalanceDataControllerTests {

    @Test
    public void solveBalanceBadRequestTest() throws IOException {

        final CloseableHttpClient httpclient = HttpClients.createDefault();

        final HttpPost httpPost = new HttpPost("http://localhost:8080/balanceData/solve");
        final List<NameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("flow", "[1,2,3]"));
        httpPost.setEntity(new UrlEncodedFormEntity(params));

        try (
                CloseableHttpResponse response2 = httpclient.execute(httpPost)
        ){
            final int statusCode = response2.getStatusLine().getStatusCode();
            Assertions.assertTrue(statusCode == 400);
        }
        httpclient.close();
    }
}
