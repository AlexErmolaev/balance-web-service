package com.noname.BalanceWebServiceLight;

import com.noname.BalanceWebServiceLight.entities.BalanceData;
import com.noname.BalanceWebServiceLight.service.BalanceDataService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class BalanceDataServiceTests {

	private boolean isSimilar(double[] a, double[] b){
		if (a.length == b.length){
			for(int i=0; i<a.length; i++){
				if (Math.abs(a[i] - b[i]) > 0.1) return false;
			}
			return true;
		}
		else return false;
	}

	private double[] RESULT_FLOW_TEST_V1 = new double[]{10.540,	2.836, 6.973, 1.963, 5.009,	4.020, 0.989, 0.731};
	private double[] RESULT_FLOW_ORIGIN = new double[]{10.056, 3.014, 7.041, 1.982,	5.059, 4.067, 0.992};

	@Test
	public void computerBalanceTestV1() {
		double[] x0 = new double[] {10.005, 3.033,	6.831, 1.985, 5.093, 4.057,	0.991, 6.667};
		double[][] A = new double[][]{
				{1, -1, -1, 0, 0, 0, 0, -1},
				{0, 0, 1, -1, -1, 0, 0, 0},
				{0,	0, 0, 0, 1, -1, -1,	0}
		};
		double[] toleranceCrude = new double[]{0.200, 0.121, 0.683,	0.040, 0.102, 0.081, 0.020,	0.667};
		BalanceData bD = new BalanceData(x0,A,toleranceCrude);
		double[] resultFlow = BalanceDataService.solveBalance(bD);
		Assertions.assertTrue(isSimilar(RESULT_FLOW_TEST_V1,resultFlow));
	}

	@Test
	public void computerBalanceTestOrigin() {
		double[] x0 = new double[] {10.005, 3.033,	6.831, 1.985, 5.093, 4.057,	0.991};
		double[][] A = new double[][]{
				{1, -1, -1, 0, 0, 0, 0},
				{0, 0, 1, -1, -1, 0, 0},
				{0,	0, 0, 0, 1, -1, -1}
		};
		double[] toleranceCrude = new double[]{0.200, 0.121, 0.683,	0.040, 0.102, 0.081, 0.020};
		BalanceData bD = new BalanceData(x0,A,toleranceCrude);
		double[] resultFlow = BalanceDataService.solveBalance(bD);
		Assertions.assertTrue(isSimilar(RESULT_FLOW_ORIGIN,resultFlow));
	}

	@Test
	public void computerBalanceTestV1_Mult_10() {
		double[] x0 = new double[] {10.005, 3.033,	6.831, 1.985, 5.093, 4.057,	0.991, 6.667};
		double[][] A = new double[][]{
				{1, -1, -1, 0, 0, 0, 0, -1},
				{0, 0, 1, -1, -1, 0, 0, 0},
				{0,	0, 0, 0, 1, -1, -1,	0},
				{-10, 1, 0, 0, 0, 0, 0,	0}
		};
		double[] toleranceCrude = new double[]{0.200, 0.121, 0.683,	0.040, 0.102, 0.081, 0.020,	0.667};

		BalanceData bD = new BalanceData(x0,A,toleranceCrude);
		double[] resultFlow = BalanceDataService.solveBalance(bD);
		Assertions.assertTrue(Math.abs(resultFlow[1]-resultFlow[0]*10) < 0.0001);
	}
}
